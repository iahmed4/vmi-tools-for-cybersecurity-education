#define _GNU_SOURCE
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <stdio.h>
#include <time.h>
#include <libvmi/libvmi.h>
#include <libvmi/nopdetect.h>


int
polycheck (unsigned char *buffer, u_int16_t length,int *cnt)
{
  u_int16_t index, nops, skipped;

  if (length < ALARM)
    return 0;

  index = 0;
  nops = 0;
  skipped = 0;
*cnt=0;

  while (index < length) {
    if (nopz[(u_char) buffer[index++]]) {
      //printf("%x", buffer[index]);
      nops++;
    }
    else if ((skipped < SKIP) && (nops!=0))
      skipped++;
    else
      {nops = 0;
      skipped = 0;}
      
//printf("\n alarms %d %d",nops,ALARM);
(*cnt)++;
    if (nops == ALARM)
    {
    printf("\nskipped %d\n",skipped);
    vmi_print_hex(buffer,length);
      return 1;
      }
      

  }

  return 0;
}


/*
 * create the hash table for the nop replacements 
 */

void
make_table (void)
{
//  int i;

  // fill in the known nops into the array
  bzero (nopz, 256);
  /*for (i = 0; i < (sizeof (IA32_nops) / sizeof (struct IA32_nop)); i++) {
    nopz[(u_char) IA32_nops[i].code] = 1;
    //printf("%x  ", (u_char) IA32_nops[i].code );
  }

  nopz[0x90] = 1;		// regular nop
  // now we add instruction prefixes which could
//     also be used as nop-replacements 
  // group 1
  nopz[0x0f] = 1;		// two byte instruction prefix
  nopz[0xf0] = 1;		// lock
  nopz[0xf2] = 1;		// repne/repnz
  nopz[0xf3] = 1;		// rep/repe/repz
  // group 2
  nopz[0x2e] = 1;		// CS segment override
  nopz[0x36] = 1;		// SS segment override
  nopz[0x3e] = 1;		// DS segment override
  nopz[0x26] = 1;		// ES segment override
  nopz[0x64] = 1;		// FS segment override
  nopz[0x65] = 1;		// GS segment override 
  // groups 3 and 4
  nopz[0x66] = 1;		// operand-size override
  nopz[0x67] = 1;		// address-size override
  */
  
    nopz[0x69] = 1;
    nopz[0x72] = 1;
    nopz[0x66] = 1;
    nopz[0x61] = 1;
    nopz[0x6e] = 1;
    nopz[0x68] = 1;
    nopz[0x6d] = 1;
    nopz[0x65] = 1;
    nopz[0x64] = 1;
    nopz[0x20] = 1;
    nopz[0x7c] = 1;
    nopz[0x37] = 1;
    nopz[0x62] = 1;
    nopz[0x24] = 1;
  
}

int
cat_my_hex (char *data, int length,uint32_t virt_address)
{
int cnt;
  if (polycheck (data, length,&cnt)) {

    printf ("\nNOP Sled Detected at %x %d",virt_address,cnt);
    return 1;
  }
  return 0;

}


uint32_t
bitsToInt (const unsigned char *bits, uint32_t offset)
{
  uint32_t result = 0;
  int n;
  for (n = sizeof (result); n >= 0; n--) {
    result = (result << 8) + bits[n + offset];
  }
  return result;
}

unsigned char
read_char (vmi_instance_t * instance, uint32_t virt_address, int pid)
{
  char *memory = NULL;
  uint32_t offset = 0;
  memory = vmi_read_str_va (*instance, virt_address, pid);
  if (memory) {
    unsigned char retval = memory[offset];
    if (memory) {
      free (memory);
      memory = NULL;
    }
    return retval;
  }
  else {
    return 0;
  }
}

uint32_t
read_space (vmi_instance_t * instance, uint32_t virt_address, int pid, int size)
{

  unsigned char *memory = NULL;
  char *data;
  uint32_t val = NULL;
  uint32_t offset = 0;
  int i;
  size_t bytes_read = 0;
  if (size == 6144)
    return 0;
  else
    //printf("\n address  to be read: 0x%x\n",virt_address);
    data = (char *) malloc (size + 1);

   vmi_read_va (*instance, virt_address, pid, size - 1, data, &bytes_read);
//  printf("\nbytes read %d\n", bytes_read );
/*  for( i=0; i < bytes_read-1; i++)
     {
     printf("%x",data[i]);
     } 

  printf("\n");
*/
  cat_my_hex (data, bytes_read,virt_address);
}



uint32_t
read_int (vmi_instance_t * instance, uint32_t virt_address, int pid)
{
  uint32_t retval;

  vmi_read_32_va (*instance, virt_address, pid, &retval);
  if (retval) {
    return retval;
  }
  else {
    return 0;
  }
}




int
main (int argc, char **argv)
{
  vmi_instance_t vmi;
  int pid = 0;
  uint32_t peb = 0;
  uint32_t dom, temp;
  unsigned i, j;
  unsigned long tasks_offset = 0, pid_offset = 0, name_offset = 0;   

  /* this is the VM that we are looking at */
  char *name = argv[1];
  /* this is the process that we are looking at */
  pid = atoi (argv[2]);

    if (VMI_FAILURE ==
            vmi_init_complete(&vmi, name, VMI_INIT_DOMAINNAME, NULL,
                              VMI_CONFIG_GLOBAL_FILE_ENTRY, NULL, NULL)) {
        printf("Failed to init LibVMI library.\n");
        return 1;
    }

    /* init the offset values */
    if (VMI_OS_LINUX == vmi_get_ostype(vmi)) {
        if ( VMI_FAILURE == vmi_get_offset(vmi, "linux_tasks", &tasks_offset) )
            goto error_exit;
        if ( VMI_FAILURE == vmi_get_offset(vmi, "linux_name", &name_offset) )
            goto error_exit;
        if ( VMI_FAILURE == vmi_get_offset(vmi, "linux_pid", &pid_offset) )
            goto error_exit;
    } else if (VMI_OS_WINDOWS == vmi_get_ostype(vmi)) {
        if ( VMI_FAILURE == vmi_get_offset(vmi, "win_tasks", &tasks_offset) )
            goto error_exit;
        if ( VMI_FAILURE == vmi_get_offset(vmi, "win_pname", &name_offset) )
            goto error_exit;
        if ( VMI_FAILURE == vmi_get_offset(vmi, "win_pid", &pid_offset) )
            goto error_exit;
    } else if (VMI_OS_FREEBSD == vmi_get_ostype(vmi)) {
        tasks_offset = 0;
        if ( VMI_FAILURE == vmi_get_offset(vmi, "freebsd_name", &name_offset) )
            goto error_exit;
        if ( VMI_FAILURE == vmi_get_offset(vmi, "freebsd_pid", &pid_offset) )
            goto error_exit;
    }

  /* pause the vm for consistent memory access */
  //vmi_pause_vm(vmi);

  /*
   * access the memory 
   */
//   printf("2\n");
  for (i = 0x7FFD0000; i <= 0x7FFDF000; i += 0x1000) {
 //    printf("3 %x %d %d\n",i,read_int (&vmi, i + 0xa4, pid),read_int (&vmi, i + 0xa8, pid));  
    if (read_int (&vmi, i + 0xa4, pid) == 6 && read_int (&vmi, i + 0xa8, pid) == 1) {
      printf("Found PEB at %x\n", i);
      peb = i;
      break;
    }
  
  }

  
  if (peb != 0) {

    make_table ();		// create hash table for nop replacements
    //time2=clock();
    for (j = 0; j < 1; j++) {

      long heap_size = 0;
      uint32_t currentHeap = 0;
      int heapCounter = 0;
      int numberOfHeaps = 0;
      int maximumNumberOfHeaps = 0;
      int beingDebugged = 0;
      int numberOfProcessors = 0;
      uint32_t processHeaps = 0;
      int tSize = 0;
	uint32_t currentEntry, lastEntry;
	uint32_t encoding;
	unsigned char encoding_flag;

      //printf("The heap is found\n");
      maximumNumberOfHeaps = read_int (&vmi, peb + 0x8c, pid);
      numberOfHeaps = read_int (&vmi, peb + 0x88, pid);
      beingDebugged = read_char (&vmi, peb + 0x002, pid);
      numberOfProcessors = read_int (&vmi, peb + 0x064, pid);
      processHeaps = read_int (&vmi, peb + 0x090, pid);

/*      printf("NumberOfHeaps: %d\n", numberOfHeaps);
      printf("MaximumNumberOfHeaps: %d\n", maximumNumberOfHeaps);
      printf("BeingDebugged: %d\n", beingDebugged);
      printf("NumberOfProcessors: %d\n", numberOfProcessors);
      printf("ProcessHeaps: %x\n", processHeaps);
*/
      for (heapCounter = 0; heapCounter < numberOfHeaps; heapCounter++) {
	uint32_t currentSegment;
	uint32_t signature;
	int segmentCounter = 0;

	//printf("HEAP #\t%d\n",heapCounter);

	currentHeap = read_int (&vmi, processHeaps + (4 * heapCounter), pid);
	// temp = read_space(&xai,currentHeap, pid);//read the whole
	// heap segment

	if (currentHeap == 0) {
	  printf ("\nBreak\n");
	  break;
	}

//_HEAP data structure

	printf("Heap:\t%d\t 0x%x\n",heapCounter, currentHeap);


//_HEAP_ENTRY data structure
//iterate heap entries in a segment
	  currentEntry = currentHeap;// read_int (&vmi, currentHeap + 0x24, pid);
	  lastEntry = read_int (&vmi, currentHeap + 0x28, pid);
	  encoding = read_int (&vmi, currentHeap + 0x50, pid);
	  encoding_flag = read_char (&vmi, currentHeap + 0x52, pid);

	  while (currentEntry< lastEntry) {
	    uint32_t size = 0, decode_size =0;
	    unsigned char flags;
	    const unsigned char LAST_ENTRY = 0x10;
	    const unsigned char CRAP_ENTRY = 0x0;


	    size = read_int (&vmi, currentEntry + 0x000, pid);
	    decode_size = size^encoding;
//	    printf("size = 0x%x, encoding = 0x%x, decode_size = 0x%x\n", size,encoding,decode_size);

	    if (size == 0) {
	      break;
	    }

	    decode_size &= 0x0000FFFF;	// 2 byte value

	    flags = read_char (&vmi, currentEntry + 0x005, pid);
	    flags = flags^encoding_flag;

	    if (flags == 0)
	      break;

	    heap_size += decode_size;
/*	    printf("            Entry: 0x%x\n", currentEntry);
	    printf("               size = 0x%x\n", decode_size * 8);
	    printf("               flags = 0x%x\n", flags);
*/
	    tSize = decode_size * 8 - 8;

/*	    if (flags & LAST_ENTRY) {
	      break;
	    }

	    if (flags == CRAP_ENTRY) {
	      break;
	    }
*/
	    //printf("%0.2x\t%0.2x\n", flags,currentEntry );

	    if (decode_size > 0 /* && tSize<=4096  */ ) {
	      temp = read_space (&vmi, currentEntry, pid, tSize);
	    }
	    currentEntry += (decode_size * 8);


	  }
      }
      // printf("Total heap size: %u bytes\n", heap_size);
    }
  }


error_exit:
  /* resume the vm */
  //vmi_resume_vm(vmi);

  /* cleanup any memory associated with the LibVMI instance */
  vmi_destroy (vmi);


  return 0;
}
