#define SKIP 1			// how many bytes to skip
#define ALARM 30		// alarm threshold

char nopz[256];			// global hash table

/* prototypes */
int polycheck(unsigned char *buffer, u_int16_t length, int*);
void make_table(void);


struct IA32_nop {
  char code;            /* the instruction */
  int bytes;            /* size of the instruction+arguments */
  char *affect;		/* what does this operation affect? */
//  int bad;		/* is the operation usable under the given blacklists? */
};
 
struct IA32_nop IA32_nops[] = {
  { '\x04',2,"EAX" },             /* add [byte],%al    */
  { '\x05',5,"EAX" },             /* add [dword],%eax  */
  { '\x06',1,"ESP" },		  /* push es           */
  { '\x0c',2,"EAX" },             /* or [byte],%al     */
  { '\x0d',5,"EAX" },             /* or [dword],%eax   */
  { '\x0e',1,"ESP" },             /* push cs           */
  { '\x14',2,"EAX" },             /* adc [byte],%al    */
  { '\x15',5,"EAX" },             /* adc [dword],%eax  */
  { '\x16',1,"ESP" },             /* push ss           */
  { '\x1c',2,"EAX" },             /* sbb [byte],%al    */
  { '\x1d',5,"EAX" },             /* sbb [dword],%eax  */
  { '\x1e',1,"ESP" },             /* push ds           */
  { '\x24',2,"EAX" },             /* and [byte],%al    */
  { '\x25',5,"EAX" },             /* and [dword],%eax  */
  { '\x27',1,"EAX" },             /* daa        '''    */
  { '\x2c',2,"EAX" },             /* sub [byte],%al    */
  { '\x2d',5,"EAX" },             /* sub [dword],%eax  */
  { '\x2f',1,"EAX" },             /* das        '/'    */
  { '\x34',2,"EAX" },             /* xor [byte],%al    */
  { '\x35',5,"EAX" },             /* xor [dword],%eax  */
  { '\x37',1,"EAX" },             /* aaa        '7'    */
  { '\x3c',2,"" },                /* cmp [byte],%al    */
  { '\x3d',5,"" },                /* cmp [dword],%eax  */
  { '\x3f',1,"EAX" },             /* aas        '?'    */
  { '\x40',1,"EAX" },             /* inc %eax   '@'    */
  { '\x41',1,"ECX" },             /* inc %ecx   'A'    */
  { '\x42',1,"EDX" },             /* inc %edx   'B'    */
  { '\x43',1,"EBX" },             /* inc %ebx   'C'    */
  { '\x44',1,"ESP" },             /* inc %esp   'D'    */
  { '\x45',1,"EBP" },             /* inc %ebp   'E'    */
  { '\x46',1,"ESI" },             /* inc %esi   'F'    */
  { '\x47',1,"EDI" },             /* inc %edi   'G'    */
  { '\x48',1,"EAX" },             /* dec %eax   'H'    */
  { '\x49',1,"ECX" },             /* dec %ecx   'I'    */
  { '\x4a',1,"EDX" },             /* dec %edx   'J'    */
  { '\x4b',1,"EBX" },             /* dec %ebx   'K'    */
  { '\x4c',1,"ESP" },             /* dec %esp   'L'    */
  { '\x4d',1,"EBP" },             /* dec %ebp   'M'    */
  { '\x4e',1,"ESI" },             /* dec %esi   'N'    */
  { '\x4f',1,"EDI" },             /* dec %edi   'O'    */
  { '\x50',1,"ESP" },             /* push %eax  'P'    */
  { '\x51',1,"ESP" },             /* push %ecx  'Q'    */
  { '\x52',1,"ESP" },             /* push %edx  'R'    */
  { '\x53',1,"ESP" },             /* push %ebx  'S'    */
  { '\x54',1,"ESP" },             /* push %esp  'T'    */
  { '\x55',1,"ESP" },             /* push %ebp  'U'    */
  { '\x56',1,"ESP" },             /* push %esi  'V'    */
  { '\x57',1,"ESP" },             /* push %edi  'W'    */
  { '\x58',1,"ESP,EAX" },         /* pop %eax   'X'    */
  { '\x59',1,"ESP,ECX" },         /* pop %ecx   'Y'    */
  { '\x5a',1,"ESP,EDX" },         /* pop %edx   'Z'    */
  { '\x5b',1,"ESP,EBX" },         /* pop %ebx   '['    */
  { '\x5d',1,"ESP,EBP" },         /* pop %ebp   ']'    */
  { '\x5e',1,"ESP,ESI" },         /* pop %esi   '^'    */
  { '\x5f',1,"ESP,EDI" },         /* pop %edi   '_'    */
  { '\x60',1,"ESP" },             /* pusha      '`'    */
  { '\x68',5,"ESP" },             /* push [dword]      */
  { '\x6a',2,"ESP" },             /* push [byte]       */
  { '\x70',2,"EIP" },             /* jo     [byte]     */
  { '\x71',2,"EIP" },             /* jno    [byte]     */
  { '\x72',2,"EIP" },             /* jc     [byte]     */
  { '\x73',2,"EIP" },             /* jnc    [byte]     */
  { '\x74',2,"EIP" },             /* jz     [byte]     */
  { '\x75',2,"EIP" },             /* jnz    [byte]     */
  { '\x76',2,"EIP" },             /* jna    [byte]     */
  { '\x77',2,"EIP" },             /* ja     [byte]     */
  { '\x78',2,"EIP" },             /* js     [byte]     */
  { '\x79',2,"EIP" },             /* jns    [byte]     */
  { '\x7a',2,"EIP" },             /* jpe    [byte]     */
  { '\x7b',2,"EIP" },             /* jpo    [byte]     */
  { '\x7c',2,"EIP" },             /* jl     [byte]     */
  { '\x7d',2,"EIP" },             /* jnl    [byte]     */
  { '\x7e',2,"EIP" },             /* jng    [byte]     */
  { '\x7f',2,"EIP" },             /* jg     [byte]     */
  { '\x91',1,"EAX,ECX" },         /* xchg %eax,%ecx    */
  { '\x92',1,"EAX,EDX" },         /* xchg %eax,%edx    */
  { '\x93',1,"EAX,EBX" },         /* xchg %eax,%ebx    */
  { '\x95',1,"EAX,EBP" },         /* xchg %eax,%ebp    */
  { '\x96',1,"EAX,ESI" },         /* xchg %eax,%esi    */
  { '\x97',1,"EAX,EDI" },         /* xchg %eax,%edi    */
  { '\x98',1,"EAX" },             /* cwtl              */
  { '\x99',1,"EDX" },             /* cltd              */
  { '\x9b',1,"" },                /* fwait             */
  { '\x9c',1,"ESP" },             /* pushf             */
  { '\x9e',1,"" },                /* sahf              */
  { '\x9f',1,"EAX" },             /* lahf              */
  { '\xa8',2,"" },                /* test [byte],%al   */
  { '\xa9',5,"" },                /* test [dword],%eax */
  { '\xb0',2,"EAX" },             /* mov [byte],%al    */
  { '\xb1',2,"ECX" },             /* mov [byte],%cl    */
  { '\xb2',2,"EDX" },             /* mov [byte],%dl    */
  { '\xb3',2,"EBX" },             /* mov [byte],%bl    */
  { '\xb4',2,"EAX" },             /* mov [byte],%ah    */
  { '\xb5',2,"ECX" },             /* mov [byte],%ch    */
  { '\xb6',2,"EDX" },             /* mov [byte],%dh    */
  { '\xb7',2,"EBX" },             /* mov [byte],%bh    */
  { '\xb8',5,"EAX" },             /* mov [dword],%eax  */
  { '\xb9',5,"ECX" },             /* mov [dword],%ecx  */
  { '\xba',5,"EDX" },             /* mov [dword],%edx  */
  { '\xbb',5,"EBX" },             /* mov [dword],%ebx  */
  { '\xbd',5,"EBP" },             /* mov [dword],%ebp  */
  { '\xbe',5,"ESI" },             /* mov [dword],%esi  */
  { '\xbf',5,"EDI" },             /* mov [dword],%edi  */
  { '\xd4',2,"EAX" },             /* aam [byte]        */
  { '\xd5',2,"EAX" },             /* aad [byte]        */
  { '\xd6',1,"EAX" },             /* salc              */
  { '\xe0',2,"ECX,EIP" },         /* loopne [byte]     */
  { '\xe1',2,"ECX,EIP" },         /* loope  [byte]     */
  { '\xe2',2,"ECX,EIP" },         /* loop   [byte]     */
  { '\xe3',2,"EIP" },             /* jecxz  [byte]     */
  { '\xeb',2,"EIP" },             /* jmp    [byte]     */
  { '\xf5',1,"" },                /* cmc               */
  { '\xf8',1,"" },                /* clc               */
  { '\xf9',1,"" },                /* stc               */
  { '\xfc',1,"" },                /* cld               */
  { '\xfd',1,"" },                /* std               */
};
