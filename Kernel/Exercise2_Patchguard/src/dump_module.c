#include <libvmi/libvmi.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <stdio.h>

#include <zlib.h>


#define PAGESIZE 1 << 12
#define PE_SIGNATURE_SIZE 4 // 0x00004550 or PE00 

//use <executable> <vmname> <modulename> <outputFileName>
//MAIN function
/*
  This program dumps a module to a file. 
*/

char* getModuleName (vmi_instance_t vmi, uint16_t len, addr_t addr)
{
    /*below is a total hack to bypass unicode support */
    int i = 0;
    uint32_t offset = 0;
    char *tmpname = malloc(len);
    char *name = malloc(len);
    if (len == vmi_read_va(vmi, addr, 0, tmpname, len)){
        memset(name, 0, len);
        for (i = 0; i < len; i++){
            if (i%2 == 0){
                name[i/2] = tmpname[i];
            }
        }
    }
    
//    if (name) free(name);
    
    if (tmpname) free(tmpname);
    return(name);
}


int main (int argc, char **argv)
{
    vmi_instance_t vmi;
    uint32_t SizeOfImage;
    addr_t next_module, list_head, DllBase;
    char *buf;
    char *modName;
    char *VMname;
    unsigned long i,j;

    /* this is the module name that we are looking at */
    char *modNameSearch = argv[2]; //"hello.sys"; //"ntoskrnl.exe";
    

    

    char filename[100];
    //printf("Enter the file name to dump %s \n", argv[2]); 
    //scanf("%s", filename);

    FILE *fp=fopen(argv[3], "a+");

    VMname = argv[1];
     /* initialize the libvmi library */
       if (VMI_FAILURE ==
        vmi_init_complete(&vmi, VMname, VMI_INIT_DOMAINNAME, NULL,
                          VMI_CONFIG_GLOBAL_FILE_ENTRY, NULL, NULL))
    {
        printf("Failed to init LibVMI library.\n");
        return 1;
    } 

      /* pause the vm for consistent memory access */
      //vmi_pause_vm(vmi);

      /* get the head of the module list */
      vmi_read_addr_ksym(vmi, "PsLoadedModuleList", &next_module);
    
    
      list_head = next_module;

  /* store the data of first VM and compare it with other VMs*/
  /* walk the module list */
      while (1){

       /* follow the next pointer */
       addr_t  tmp_next = 0;
       vmi_read_addr_va(vmi, next_module, 0, &tmp_next);

       /* if we are back at the list head, we are done */
       if (list_head == tmp_next){
       break;
       }
      
           /*LDR_DATA_TABLE_ENTRY : the data structure has _UNICODE_STRING 
           struct at 0x2c offset. This struct contains module name */
            uint16_t length;
           addr_t buffer_addr;
      
           /*LDR_DATA_TABLE_ENTRY : the data structure has _UNICODE_STRING 
           struct at 0x2c offset. This struct contains module name */
           vmi_read_va(vmi, next_module + 0x58, 0, &length, 2);
           vmi_read_va(vmi, next_module + 0x60, 0, &buffer_addr, 8);
           modName = getModuleName(vmi, length, buffer_addr);
          


       if(!strcmp(modName,modNameSearch))
       {
              /* LDR_DATA_TABLE_ENTRY : the data structure 
              has the base address of module 
              at 0x018 offset and the module size at 0x020 offset */
          vmi_read_addr_va(vmi, next_module + 0x030, 0, &DllBase);
          vmi_read_32_va(vmi, next_module + 0x040, 0, &SizeOfImage);
       
        //fprintf( fp, "\nmodName:%s next_module:%16llx DllBase: %16llx SizeOfImage:%d\n",       
        //  modName,  next_module,   DllBase,   SizeOfImage);
       
     
        /* Read the whole module to a buffer */
        buf = (char *) malloc (SizeOfImage);
        vmi_pause_vm(vmi);
        unsigned int NoOfPagesToRead = (SizeOfImage)/(PAGESIZE);
        
          for(i=0; i<NoOfPagesToRead; i++){
            vmi_read_va(vmi, DllBase+(i*PAGESIZE ), 0, buf+(i*PAGESIZE ), PAGESIZE ); 
            
        //    printf("%x %x %x %x %x\n",DllBase[firstVM],buf,i*PAGESIZE,DllBase[firstVM]+(i*PAGESIZE ),buf+(i*PAGESIZE ));
            //vmi_print_hex(buf+(i*PAGESIZE ),PAGESIZE);
          }

        //    printf("%x %x %x %x %x\n",DllBase[firstVM],buf,i*PAGESIZE,DllBase[firstVM]+(i*PAGESIZE ),buf+(i*PAGESIZE ));
            //vmi_print_hex(buf+(i*PAGESIZE ),PAGESIZE);
        
         fwrite(buf, SizeOfImage,1 , fp );
         vmi_resume_vm(vmi);
         //vmi_print_hex(buf,SizeOfImage);
        }      //vmi_print_hex(buf,SizeOfImage);
          

          //fprintf(fp, buf);

          next_module = tmp_next;

       if (modName) free(modName);

      
      }
       
        
      error_exit:
     
      // resume the vm  
      //vmi_resume_vm(vmi);

      // cleanup any memory associated with the libvmi instance 
      vmi_destroy(vmi);
  

  fclose(fp);    

 
 

 return 0;
 
}
