import os, sys
import hashlib
import md5

def sh (script):
	os.system("bash -c '%s'" %script)

def excmd(cmd):
	os.system(cmd)


modcheckeroutput = open(sys.argv[1], "w")
"""
Open the list of modules. Then in each line in the list, dump the respective module. Calculate 
the MD5. If the MD5 of the previous hash and the new hash has changed then the kernel module 
has been patched. To demonstrate this, run Kernel_change on a module which isn't ntoskrnl.exe, Hal.dll, Ci.dll, 
kdcom.dll, pshed.dll, clfs.sys, Ndis.sys, TCPip.sys. 
"""


with open("module-list.txt") as module_file:
	for line in module_file:
		cwd = os.getcwd()
		line = line.strip('\n')
		dump = 'sudo ' + cwd + '/dump_module win7 ' + line + ' ' + os.path.splitext(line)[0] +'.bin'
		sh(dump)
		f1 = file (cwd + '/' +os.path.splitext(line)[0]+'.bin', 'rb')
		f2 = file (cwd + "/original/" +os.path.splitext(line)[0]+'.bin', 'rb')
		modcheckeroutput.write(line + ' : Integrity status ' + str(md5.new(f1.read()).digest() == md5.new(f2.read()).digest())+'\n')
		sh("mv *.bin ./tmp/ ")
		if (md5.new(f1.read()).digest() == md5.new(f2.read()).digest()):
			print (line + ' : Integrity status ' + str(md5.new(f1.read()).digest() == md5.new(f2.read()).digest())+'\n')

		