#include <string.h>

void not_called() {
    printf("Not quite a shell...\n");
    system("C:\\Windows\\System32\\calc.exe");
    system("sudo /bin/sh");
}

void vulnerable_function(char* string) {
    char buffer[100];
    getch();
    strcpy(buffer, string);
}

int main(int argc, char** argv) {
    vulnerable_function(argv[1]);
    return 0;
}